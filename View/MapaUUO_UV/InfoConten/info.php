<?php


require_once $_SERVER['DOCUMENT_ROOT'] .'/mapacartograficoouu_uv/Controller/cInfo.php';






$op = $_POST['op'];
$lon = $_POST['lon'];
$lan = $_POST['lan'];






?>

<div class="contenedo-carosel">
    <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
        <div class="carousel-inner">
            <div class="carousel-item active">
                <img class=" img-fluid img-c" src="Asset/Mapa/img/Galeria/Manzanas/marcador1/B.jpg" alt="First slide">
            </div>
            <div class="carousel-item">
                <img class=" img-fluid img-c" src="Asset/Mapa/img/Galeria/Manzanas/marcador1/C.jpg" alt="Second slide">
            </div>
            <div class="carousel-item">
                <img class=" img-fluid img-c" src="Asset/Mapa/img/Galeria/Manzanas/marcador1/D.jpg" alt="Third slide">
            </div>
        </div>
        <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>
</div>

<div class="informacion">
    <div class="card">
        <div class="card-header text-center">
            Información
        </div>
        <br>
        <div class="contenedor-tablas">

            <?php if ($op['catalogo1981'] === 'false'): ?>

            <?php else:?>
            <div class="col-12 ">
                <p class=" text-center"><strong>Catalogo 1987</strong> </p>
                <div class="col-12  text-center">
                    <div class="table-responsive ">
                        <table class="table table-sm font-table">
                            <tbody>
                            <tr>
                                <th>Folio</th>
                                <td><?php echo strtolower($info[0]['folio'])?></td>
                            </tr>
                            <tr>
                                <th scope="row">Dirección</th>
                                <td><?php echo strtolower($info[0]['location'])?></td>
                            </tr>
                            <tr>
                                <th scope="row">Nominación</th>
                                <td><?php echo strtolower($info[0]['nominacion'])?></td>
                            </tr>
                            <tr>
                                <th scope="row">Motivos</th>
                                <td colspan="2"><?php echo strtolower($info[0]['motivos'])?></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <?php endif;?>

            <?php if ($op['listado1984'] === 'false'): ?>

            <?php else:?>
            <div class="col-12 ">
                <p class=" text-center"> <strong>Listado 1984 </strong> </p>
                <div class="col-12  text-center">
                    <div class="table-responsive ">
                        <table class="table table-sm font-table">
                            <tbody>
                            <tr>
                                <th>Folio</th>
                                <td><?php echo strtolower($info[0]['folio'])?></td>
                            </tr>
                            <tr>
                                <th scope="row">Dirección</th>
                                <td><?php echo strtolower($info[0]['location'])?></td>
                            </tr>
                            <tr>
                                <th scope="row">Sección (Region/Manzana) </th>
                                <td><?php echo strtolower($info[0]['seccion'])?></td>
                            </tr>
                            <tr>
                                <th scope="row">Clasificación</th>
                                <td colspan="2"><?php echo strtolower($info[0]['clasificacion'])?></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <?php endif;?>

            <?php if ($op['declaratoria1990'] === 'false'): ?>

            <?php else:?>
            <div class="col-12 ">
                <p class=" text-center"><strong>Declaratoria1990</strong> </p>
                <div class="col-12  text-center">
                    <div class="table-responsive ">
                        <table class="table table-sm font-table">
                            <tbody>
                            <tr>
                                <th>Folio</th>
                                <td><?php echo strtolower($info[0]['folio'])?></td>
                            </tr>
                            <tr>
                                <th scope="row">Dirección</th>
                                <td><?php echo strtolower($info[0]['location'])?></td>
                            </tr>
                            <tr>
                                <th scope="row">Denominación </th>
                                <td><?php echo strtolower($info[0]['denominacion'])?></td>
                            </tr>
                            <tr>
                                <th scope="row">Fecha de construcción</th>
                                <td colspan="2"><?php echo strtolower($info[0]['siglo'])?></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <?php endif;?>

            <?php if ($op['catalogo2006'] === 'false'): ?>

            <?php else:?>
            <div class="col-12 ">
                <p class=" text-center"><strong> Catalogo 2006 </strong> </p>
                <div class="col-12  text-center">
                    <div class="table-responsive ">
                        <table class="table table-sm font-table">
                            <tbody>
                                <tr>
                                    <th>Folio</th>
                                    <td><?php echo strtolower($info[0]['folio'])?></td>
                                </tr>
                                <tr>
                                    <th scope="row">Dirección</th>
                                    <td><?php echo strtolower($info[0]['location'])?></td>
                                </tr>
                                <tr>
                                    <th scope="row">Fecha de construcción</th>
                                    <td colspan="2"><?php echo strtolower($info[0]['siglo'])?></td>
                                </tr>

                                <tr>
                                    <th scope="row">Clasificaón  </th>
                                    <td colspan="2"><?php echo strtolower($info[0]['classificacion'])?></td>
                                </tr>

                                <tr>
                                    <th scope="row">Genero Arquitectonico Original</th>
                                    <td colspan="2"><?php echo strtolower($info[0]['gen_arq_ori'])?></td>
                                </tr>
                                <tr>
                                    <th scope="row">Uso original</th>
                                    <td colspan="2"><?php echo strtolower($info[0]['uso_orig'])?></td>
                                </tr>
                                <tr>
                                    <th scope="row">Uso actual </th>
                                    <td colspan="2"><?php echo strtolower($info[0]['uso_actual'])?></td>
                                </tr>
                                <tr>
                                    <th scope="row">Estatus de protección</th>
                                    <td colspan="2"><?php echo strtolower($info[0]['estatus_proteccion'])?></td>
                                </tr>
                                <tr>
                                    <th scope="row">Estado de conservación</th>
                                    <td colspan="2"><?php echo strtolower($info[0]['estado_conservacion']) ?></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <?php endif;?>

        </div>
    </div>
</div>