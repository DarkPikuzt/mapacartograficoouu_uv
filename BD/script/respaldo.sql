/*
SQLyog Community v13.1.6 (64 bit)
MySQL - 5.7.24 : Database - cuo-mapa-cartografico
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`cuo-mapa-cartografico` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `cuo-mapa-cartografico`;

/*Table structure for table `catalogue1981` */

DROP TABLE IF EXISTS `catalogue1981`;

CREATE TABLE `catalogue1981` (
  `id_catalogue` int(11) NOT NULL AUTO_INCREMENT,
  `folio` varchar(250) DEFAULT NULL,
  `nominacion` varchar(250) DEFAULT NULL,
  `motivos` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`id_catalogue`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

/*Data for the table `catalogue1981` */

insert  into `catalogue1981`(`id_catalogue`,`folio`,`nominacion`,`motivos`) values 
(1,'300870001-0001','Catedral ','Patrimonio Nacional '),
(2,'300870001-0002','Casa habitación histórica ','Sistema constructivo del siglo pasado'),
(3,'300870001-0012','Casa habitación artística ','Relevancia en su sistema constructivo de principios de siglo '),
(4,'300870001-0015','Casa habitación artística','Relevancia en su sistema constructivo de principios de siglo '),
(5,'300870001-25','Cuartel de San José Histórico  ','Fueron fusiladoos el 24 de Nov. De 1847 Alcalde y García');

/*Table structure for table `catalogue2006` */

DROP TABLE IF EXISTS `catalogue2006`;

CREATE TABLE `catalogue2006` (
  `id_ca_2006` int(11) NOT NULL AUTO_INCREMENT,
  `siglo` varchar(250) DEFAULT NULL,
  `classificacion` varchar(250) DEFAULT NULL,
  `gen_arq_ori` varchar(250) DEFAULT NULL,
  `uso_orig` varchar(250) DEFAULT NULL,
  `uso_actual` varchar(250) DEFAULT NULL,
  `estatus_proteccion` varchar(250) DEFAULT NULL,
  `estado_conservacion` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`id_ca_2006`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `catalogue2006` */

/*Table structure for table `declaration` */

DROP TABLE IF EXISTS `declaration`;

CREATE TABLE `declaration` (
  `id_declaration` int(11) NOT NULL AUTO_INCREMENT,
  `folio` varchar(250) DEFAULT NULL,
  `denominacion` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`id_declaration`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `declaration` */

/*Table structure for table `direction` */

DROP TABLE IF EXISTS `direction`;

CREATE TABLE `direction` (
  `id_direction` int(11) NOT NULL AUTO_INCREMENT,
  `location` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_direction`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `direction` */

/*Table structure for table `information` */

DROP TABLE IF EXISTS `information`;

CREATE TABLE `information` (
  `id_inf` int(11) NOT NULL AUTO_INCREMENT,
  `id_markers` int(11) DEFAULT NULL,
  `id_1984` int(11) DEFAULT NULL,
  `id_1981` int(11) DEFAULT NULL,
  `id_2006` int(11) DEFAULT NULL,
  `id_declaratoria` int(11) DEFAULT NULL,
  `id_direccion` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_inf`),
  KEY `id_markers` (`id_markers`),
  KEY `id_1984` (`id_1984`),
  KEY `id_1981` (`id_1981`),
  KEY `id_2006` (`id_2006`),
  KEY `id_declaratoria` (`id_declaratoria`),
  KEY `id_direccion` (`id_direccion`),
  CONSTRAINT `information_ibfk_1` FOREIGN KEY (`id_markers`) REFERENCES `markers` (`id`),
  CONSTRAINT `information_ibfk_2` FOREIGN KEY (`id_1984`) REFERENCES `list1984` (`id_list`),
  CONSTRAINT `information_ibfk_3` FOREIGN KEY (`id_1981`) REFERENCES `catalogue1981` (`id_catalogue`),
  CONSTRAINT `information_ibfk_4` FOREIGN KEY (`id_2006`) REFERENCES `catalogue2006` (`id_ca_2006`),
  CONSTRAINT `information_ibfk_5` FOREIGN KEY (`id_declaratoria`) REFERENCES `declaration` (`id_declaration`),
  CONSTRAINT `information_ibfk_6` FOREIGN KEY (`id_direccion`) REFERENCES `direction` (`id_direction`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `information` */

/*Table structure for table `list1984` */

DROP TABLE IF EXISTS `list1984`;

CREATE TABLE `list1984` (
  `id_list` int(11) NOT NULL AUTO_INCREMENT,
  `folio` varchar(100) DEFAULT NULL,
  `seccion` varchar(100) DEFAULT NULL,
  `clasificacion` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id_list`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `list1984` */

/*Table structure for table `markers` */

DROP TABLE IF EXISTS `markers`;

CREATE TABLE `markers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Longitud` double DEFAULT NULL,
  `Latitud` double DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

/*Data for the table `markers` */

insert  into `markers`(`id`,`Longitud`,`Latitud`) values 
(1,-96.923144,19.528065),
(2,-96.923151,19.528194),
(3,-96.923459,19.528264),
(4,-96.923482,19.528414),
(5,-96.916487,19.530644);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
