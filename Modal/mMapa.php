<?php
include_once $_SERVER['DOCUMENT_ROOT'] .'/mapacartograficoouu_uv/Config/DB.php';


class mMapa
{

    public $id;


    public function getId()
    {
        return $this->id;
    }


    public function setId($id)
    {
        $this->id = $id;
    }

    public function marcadores(){
        $conexion = new DB();
        $conn = $conexion->connection();
        $sql = "       
        select m2.id, m2.Latitud, m2.Longitud , i.folio from information i 
        left join markers m2 on i.id_markers = m2.id        
        ";

        $puntos = $conn->prepare($sql);
        $puntos->execute();
        $result = $puntos->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    public function infomation(){
        $id = $this->getId();
        $conexion = new DB();
        $conn = $conexion->connection();
        $sql = "select * from information i 
left join catalogue1981 c on i.id_1981  = c.id_catalogue
left join catalogue2006 c2 on i.id_2006  = c2.id_ca_2006
left join direction d on i.id_direccion = d.id_direction where i.id_markers = '$id'";


        //var_dump($sql);
        $inf = $conn->prepare($sql);
        $inf->execute();

        //var_dump($inf);
        $result = $inf->fetchAll(PDO::FETCH_ASSOC);

        return $result;
    }




}