/*Marcadores */
function manzanas(map){
    markers = [];
    for (i = 0; i < puntos.length; i++) {
        //console.log(puntos[i])
        var position = new google.maps.LatLng(manzanas[i]['Latitud'], manzanas[i]['Longitud']);
        // /*Marcador*/
        let marker = new google.maps.Marker({
            position: position,
            icon: '/img/m1.png',
            icon: {
                path: google.maps.SymbolPath.CIRCLE,
                scale: 4, //tamaño
                strokeColor: '#FFFFFF', //color del borde
                strokeWeight: .5, //grosor del borde
                fillColor: '#63B2D3', //color de relleno
                fillOpacity: 1 // opacidad del relleno
            },
            map: map,
            title: manzanas[i]['nombre']
        });
        markers.push(marker);
        o_manzanas.push(marker);
        (function(i, marker) {
            google.maps.event.addListener(marker, 'click', function() {
                let infowindow;
                if (!infowindow) {
                    infowindow = new google.maps.InfoWindow({
                        maxWidth: 600
                    });
                }

                let url = "View/MapaUUO_UV/infoConten/info.php";
                let street = "View/MapaUUO_UV/infoConten/streetView.php";
                let  indice = manzanas[i]['id'];
                let  lon = manzanas[i]['Longitud'];
                let  lan = manzanas[i]['Latitud'];

                //alert(indice)

                $.ajax({
                    data: {id:indice,op:MenuInnmuebles, lon:lon,lan:lan},
                    type: "POST",
                    dataType: "html",
                    url: url,
                    success:function (data) {
                        infowindow.setContent(data);
                        infowindow.open(map, marker);
                    }
                });


                map.setZoom(20);
                map.setCenter(marker.getPosition());
            });
        })(i, marker);
    }

}

function deleteManzanas(map) {
    //console.log(O_array_E_AutoConsumo);
    for (let i = 0; i < o_manzanas.length; i++) {
        o_manzanas[i].setMap(map);

    }
}
function ocultarM() {
    deleteManzanas(null);
}



function catalogo1981(map){
    markers = [];
    for (i = 0; i < puntos.length; i++) {
        //console.log(puntos[i])
        var position = new google.maps.LatLng(puntos[i]['Latitud'], puntos[i]['Longitud']);
        // /*Marcador*/
        let marker = new google.maps.Marker({
            position: position,
            icon: '/img/m1.png',
            icon: {
                path: google.maps.SymbolPath.CIRCLE,
                scale: 8, //tamaño
                strokeColor: '#FFFFFF', //color del borde
                strokeWeight: .5, //grosor del borde
                fillColor: '#FB9931', //color de relleno
                fillOpacity: 1 // opacidad del relleno
            },
            map: map,
            title: puntos[i]['nombre']
        });
        markers.push(marker);
        (function(i, marker) {
            google.maps.event.addListener(marker, 'click', function() {
                let infowindow;
                if (!infowindow) {
                    infowindow = new google.maps.InfoWindow({
                        maxWidth: 600
                    });
                }
                let url = "View/MapaUUO_UV/infoConten/info.php";
                let street = "View/MapaUUO_UV/infoConten/streetView.php";
                let  indice = puntos[i]['id'];
                let  lon = puntos[i]['Longitud'];
                let  lan = puntos[i]['Latitud'];

                //alert(indice)

                $.ajax({
                    data: {id:indice,op:MenuInnmuebles, lon:lon,lan:lan},
                    type: "POST",
                    dataType: "html",
                    url: url,
                    success:function (data) {
                        infowindow.setContent(data);
                        infowindow.open(map, marker);
                    }
                });


                map.setZoom(20);
                map.setCenter(marker.getPosition());
            });
        })(i, marker);
    }

}
function listadi1984(map){
    markers = [];
    for (i = 0; i < puntos.length; i++) {
        //console.log(puntos[i])
        var position = new google.maps.LatLng(puntos[i]['Latitud'], puntos[i]['Longitud']);
        // /*Marcador*/
        let marker = new google.maps.Marker({
            position: position,
            icon: '/img/m1.png',
            icon: {
                path: google.maps.SymbolPath.CIRCLE,
                scale: 12, //tamaño
                strokeColor: '#FFFFFF', //color del borde
                strokeWeight: .5, //grosor del borde
                fillColor: '#07CD21', //color de relleno
                fillOpacity: 1 // opacidad del relleno
            },
            map: map,
            title: puntos[i]['nombre']
        });
        markers.push(marker);
        (function(i, marker) {
            google.maps.event.addListener(marker, 'click', function() {
                let infowindow;
                if (!infowindow) {
                    infowindow = new google.maps.InfoWindow({
                        maxWidth: 600
                    });
                }
                let url = "View/MapaUUO_UV/infoConten/info.php";
                let street = "View/MapaUUO_UV/infoConten/streetView.php";
                let  indice = puntos[i]['id'];
                let  lon = puntos[i]['Longitud'];
                let  lan = puntos[i]['Latitud'];

                //alert(indice)

                $.ajax({
                    data: {id:indice,op:MenuInnmuebles, lon:lon,lan:lan},
                    type: "POST",
                    dataType: "html",
                    url: url,
                    success:function (data) {
                        infowindow.setContent(data);
                        infowindow.open(map, marker);
                    }
                });


                map.setZoom(20);
                map.setCenter(marker.getPosition());
            });
        })(i, marker);
    }

}
function declaratoria1990(map){
    markers = [];
    for (i = 0; i < puntos.length; i++) {
        //console.log(puntos[i])
        var position = new google.maps.LatLng(puntos[i]['Latitud'], puntos[i]['Longitud']);
        // /*Marcador*/
        let marker = new google.maps.Marker({
            position: position,
            icon: '/img/m1.png',
            icon: {
                path: google.maps.SymbolPath.CIRCLE,
                scale: 16, //tamaño
                strokeColor: '#FFFFFF', //color del borde
                strokeWeight: .5, //grosor del borde
                fillColor: '#FF0000', //color de relleno
                fillOpacity: 1 // opacidad del relleno
            },
            map: map,
            title: puntos[i]['nombre']
        });
        markers.push(marker);
        (function(i, marker) {
            google.maps.event.addListener(marker, 'click', function() {
                let infowindow;
                if (!infowindow) {
                    infowindow = new google.maps.InfoWindow({
                        maxWidth: 600
                    });
                }
                let url = "View/MapaUUO_UV/infoConten/info.php";
                let street = "View/MapaUUO_UV/infoConten/streetView.php";
                let  indice = puntos[i]['id'];
                let  lon = puntos[i]['Longitud'];
                let  lan = puntos[i]['Latitud'];

                //alert(indice)

                $.ajax({
                    data: {id:indice,op:MenuInnmuebles, lon:lon,lan:lan},
                    type: "POST",
                    dataType: "html",
                    url: url,
                    success:function (data) {
                        infowindow.setContent(data);
                        infowindow.open(map, marker);
                    }
                });


                map.setZoom(20);
                map.setCenter(marker.getPosition());
            });
        })(i, marker);
    }

}
function catalogo2006(map){
    markers = [];
    for (i = 0; i < puntos.length; i++) {
        //console.log(puntos[i])
        var position = new google.maps.LatLng(puntos[i]['Latitud'], puntos[i]['Longitud']);
        // /*Marcador*/
        let marker = new google.maps.Marker({
            position: position,
            icon: '/img/m1.png',
            icon: {
                path: google.maps.SymbolPath.CIRCLE,
                scale: 20, //tamaño
                strokeColor: '#FFFFFF', //color del borde
                strokeWeight: .5, //grosor del borde
                fillColor: '#0076F7', //color de relleno
                fillOpacity: 1 // opacidad del relleno
            },
            map: map,
            title: puntos[i]['nombre']
        });
        markers.push(marker);
        (function(i, marker) {
            google.maps.event.addListener(marker, 'click', function() {
                let infowindow;
                if (!infowindow) {
                    infowindow = new google.maps.InfoWindow({
                        maxWidth: 600
                    });
                }
                let url = "View/MapaUUO_UV/infoConten/info.php";
                let street = "View/MapaUUO_UV/infoConten/streetView.php";
                let  indice = puntos[i]['id'];
                let  lon = puntos[i]['Longitud'];
                let  lan = puntos[i]['Latitud'];

                //alert(indice)

                $.ajax({
                    data: {id:indice,op:MenuInnmuebles, lon:lon,lan:lan},
                    type: "POST",
                    dataType: "html",
                    url: url,
                    success:function (data) {
                        infowindow.setContent(data);
                        infowindow.open(map, marker);
                    }
                });


                map.setZoom(20);
                map.setCenter(marker.getPosition());
            });
        })(i, marker);
    }

}





function Puntos(map) {
    markers = [];
    for (i = 0; i < puntos.length; i++) {
        //console.log(puntos[i])
        var position = new google.maps.LatLng(puntos[i]['Latitud'], puntos[i]['Longitud']);


        /*
        *
        * position: position,
            icon: '/img/m1.png',
            icon: {
                path: google.maps.SymbolPath.CIRCLE,
                scale: 5, //tamaño
                strokeColor: 'rgba(255, 255, 255, 1)', //color del borde
                strokeWeight: .5, //grosor del borde
                fillColor: 'rgba(209, 0, 73, 1)', //color de relleno
                fillOpacity: 1 // opacidad del relleno
            },
            map: map,
            title: arrego_pf_pm[i]['nombre']
            *
            * position: position,
            icon: image,
            draggable:true,
            animation: google.maps.Animation.DROP,
            map: map,
            title: puntos[i]['nombre']
        *
        * */

        // /*Marcador*/
        let marker = new google.maps.Marker({
            position: position,
            icon: '/img/m1.png',
            icon: {
                path: google.maps.SymbolPath.CIRCLE,
                scale: 10, //tamaño
                strokeColor: 'rgba(255, 255, 255, 1)', //color del borde
                strokeWeight: .5, //grosor del borde
                fillColor: 'rgba(209, 0, 73, 1)', //color de relleno
                fillOpacity: 1 // opacidad del relleno
            },
            map: map,
            title: puntos[i]['nombre']
        });
        markers.push(marker);


        (function(i, marker) {
            google.maps.event.addListener(marker, 'click', function() {
                let infowindow;
                if (!infowindow) {
                    infowindow = new google.maps.InfoWindow({
                        maxWidth: 600
                    });
                }

                let url = "View/MapaUUO_UV/infoConten/info.php";
                let street = "View/MapaUUO_UV/infoConten/streetView.php";
                let  indice = puntos[i]['id'];
                let  lon = puntos[i]['Longitud'];
                let  lan = puntos[i]['Latitud'];

                //alert(indice)

                $.ajax({
                    data: {id:indice,op:MenuInnmuebles, lon:lon,lan:lan},
                    type: "POST",
                    dataType: "html",
                    url: url,
                    success:function (data) {
                        infowindow.setContent(data);
                        infowindow.open(map, marker);
                    }
                });


                map.setZoom(10);
                map.setCenter(marker.getPosition());
            });
        })(i, marker);


    }
    markerClusterer = new MarkerClusterer(map, markers, {imagePath: 'https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m'});
}

