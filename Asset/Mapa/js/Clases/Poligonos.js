
const municipioXalapa = "Asset/Mapa/js/Poligonos/estados/xalapa.json";
const states= "Asset/Mapa/js/Poligonos/estados/EstMex.geojson";

/*Poligonos estados*/
function statesMex(map) {
    let datastates = new google.maps.Data();
    datastates.loadGeoJson(states);
    $("#estados").on('change', function(){
        if ($(this).is(':checked') ) {
            datastates.setMap(map);
            datastates.setStyle({
                fillColor: "green",
                strokeWeight: 1
            });
        }else{
            datastates.setMap(null);
        }
    });
}

function PonMun(map) {
    let datastatesM = new google.maps.Data();
    datastatesM.loadGeoJson(municipioXalapa);
    $("#municipios").on('change', function(){
        if ($(this).is(':checked') ) {
            datastatesM.setMap(map);
            datastatesM.setStyle({
                fillColor: "blue",
                strokeWeight: 1
            });
        }else{
            datastatesM.setMap(null);
        }
    });
}

function proyectoUno(map) {
    let datastatesM = new google.maps.Data();
    datastatesM.loadGeoJson(municipioXalapa);

    let activo = false;






        $("#proyectoUno").on('click', function(){
            //alert(MenuInnmuebles)
            if (activo === false) {

                activo = true;

                if (MenuInnmuebles["catalogo1981"] == true && MenuInnmuebles["listado1984"] == true ){
                    listadi1984(map)
                    catalogo1981(map)
                }
                /*


                if(MenuInnmuebles["catalogo1981"] == true){
                    catalogo1981(map)
                }else{
                    ocultar_1981()
                }
                if(MenuInnmuebles["listado1984"] == true){
                    listadi1984(map)
                }else{
                    ocultar_1984()
                }
                if(MenuInnmuebles["declaratoria1990"] == true){
                   declaratoria1990(map)
                }else{
                    ocultar_1990()
                }

                if(MenuInnmuebles["catalogo2006"] == true){
                    catalogo2006(map)
                }else{
                    ocultar_2006()
                }

                 */






            }else{
                activo = false;
                //markerClusterer.clearMarkers();
                ocultar_1981()
                ocultar_1984()
                ocultar_1990()
                ocultar_2006()



            }
        });
}



function menureal() {
    $("#1981").on('change', function(){
        if ($(this).is(':checked') ) {

            if ( MenuInnmuebles["catalogo2006"] == true ){
                catalogo2006(map)

            }
            if (MenuInnmuebles["declaratoria1990"] == true  ){

                declaratoria1990(map)

            }
            if (MenuInnmuebles["listado1984"] == true ){
                listadi1984(map)

            }
            if (MenuInnmuebles["catalogo1981"] == true ){
                catalogo1981(map)
            }









        }else{
            ocultar_1981()

        }
    });

    $("#1984").on('change', function(){
        if ($(this).is(':checked') ) {
            if ( MenuInnmuebles["catalogo2006"] == true ){
                catalogo2006(map)

            }
            if (MenuInnmuebles["declaratoria1990"] == true  ){

                declaratoria1990(map)

            }
            if (MenuInnmuebles["listado1984"] == true ){
                listadi1984(map)

            }
            if (MenuInnmuebles["catalogo1981"] == true ){
                catalogo1981(map)
            }
        }else{

            ocultar_1984()
        }
    });

    $("#1990").on('change', function(){
        if ($(this).is(':checked') ) {
            if ( MenuInnmuebles["catalogo2006"] == true ){
                catalogo2006(map)

            }
            if (MenuInnmuebles["declaratoria1990"] == true  ){

                declaratoria1990(map)

            }
            if (MenuInnmuebles["listado1984"] == true ){
                listadi1984(map)

            }
            if (MenuInnmuebles["catalogo1981"] == true ){
                catalogo1981(map)
            }
        }else{

            ocultar_1990()

        }
    });

    $("#2006").on('change', function(){
        if ($(this).is(':checked') ) {
            if ( MenuInnmuebles["catalogo2006"] == true ){
                catalogo2006(map)
            }
            if (MenuInnmuebles["declaratoria1990"] == true  ){
                declaratoria1990(map)
            }
            if (MenuInnmuebles["listado1984"] == true ){
                listadi1984(map)
            }
            if (MenuInnmuebles["catalogo1981"] == true ){
                catalogo1981(map)
            }
        }else{
            ocultar_2006()
        }
    });




}


if (MenuInnmuebles["catalogo2006"] === true){
    catalogo2006(map);
}

