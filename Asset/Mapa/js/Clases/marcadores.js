

/*Marcadores */
function catalogo1981(map){
    markers = [];
    for (i = 0; i < catalogo1981_.length; i++) {
        //console.log(puntos[i])
        var position = new google.maps.LatLng(catalogo1981_[i]['Latitud'], catalogo1981_[i]['Longitud']);
        // /*Marcador*/
        let marker = new google.maps.Marker({
            position: position,
            icon: '/img/m1.png',
            icon: {
                path: google.maps.SymbolPath.CIRCLE,
                scale: 8, //tamaño
                strokeColor: '#FFFFFF', //color del borde
                strokeWeight: .5, //grosor del borde
                fillColor: '#FB9931', //color de relleno
                fillOpacity: 1 // opacidad del relleno
            },
            map: map,
            title: catalogo1981_[i]['nombre']
        });
        markers.push(marker);
        o_catalogo1981.push(marker);
        (function(i, marker) {
            google.maps.event.addListener(marker, 'click', function() {
                let infowindow;
                if (!infowindow) {
                    infowindow = new google.maps.InfoWindow({
                        maxWidth: 600
                    });
                }

                let  informacion = 'Folio:'+ catalogo2006_[i]['folio'] ;
                infowindow.open(map, marker)
                infowindow.setContent(informacion);
                let url = "View/MapaUUO_UV/infoConten/info.php";
                let street = "View/MapaUUO_UV/infoConten/streetView.php";
                let  indice = catalogo1981_[i]['id'];
                let  lon = catalogo1981_[i]['Longitud'];
                let  lan = catalogo1981_[i]['Latitud'];

                //alert(indice)

                $.ajax({
                    data: {id:indice,op:MenuInnmuebles, lon:lon,lan:lan},
                    type: "POST",
                    dataType: "html",
                    url: url,
                    success:function (data) {
                        myFunction(indice,lon,lan);
                        map.setZoom(20);
                        map.setCenter(marker.getPosition());

                    }
                });


                map.setZoom(20);
                map.setCenter(marker.getPosition());
            });
        })(i, marker);
    }

}
function listadi1984(map){
    markers = [];
    for (i = 0; i < listado1984_.length; i++) {
        //console.log(puntos[i])
        var position = new google.maps.LatLng(listado1984_[i]['Latitud'], listado1984_[i]['Longitud']);
        // /*Marcador*/
        let marker = new google.maps.Marker({
            position: position,
            icon: '/img/m1.png',
            icon: {
                path: google.maps.SymbolPath.CIRCLE,
                scale: 12, //tamaño
                strokeColor: '#FFFFFF', //color del borde
                strokeWeight: .5, //grosor del borde
                fillColor: '#07CD21', //color de relleno
                fillOpacity: 1 // opacidad del relleno
            },
            map: map,
            title: listado1984_[i]['nombre']
        });
        markers.push(marker);
        o_listado1984.push(marker);
        (function(i, marker) {
            google.maps.event.addListener(marker, 'click', function() {
                let infowindow;
                if (!infowindow) {
                    infowindow = new google.maps.InfoWindow({
                        maxWidth: 600
                    });
                }

                let  informacion = 'Folio:'+ catalogo2006_[i]['folio'] ;
                infowindow.open(map, marker)
                infowindow.setContent(informacion);
                let url = "View/MapaUUO_UV/infoConten/info.php";
                let street = "View/MapaUUO_UV/infoConten/streetView.php";

                let  indice = listado1984_[i]['id'];
                let  lon = listado1984_[i]['Longitud'];
                let  lan = listado1984_[i]['Latitud'];

                //alert(indice)

                $.ajax({
                    data: {id:indice,op:MenuInnmuebles, lon:lon,lan:lan},
                    type: "POST",
                    dataType: "html",
                    url: url,
                    success:function (data) {
                        myFunction(indice,lon,lan);
                        map.setZoom(20);
                        map.setCenter(marker.getPosition());

                    }
                });


                map.setZoom(20);
                map.setCenter(marker.getPosition());
            });
        })(i, marker);
    }

}
function declaratoria1990(map){
    markers = [];
    for (i = 0; i < declaratoria1990_.length; i++) {
        //console.log(puntos[i])
        var position = new google.maps.LatLng(declaratoria1990_[i]['Latitud'], declaratoria1990_[i]['Longitud']);
        // /*Marcador*/
        let marker = new google.maps.Marker({
            position: position,
            icon: '/img/m1.png',
            icon: {
                path: google.maps.SymbolPath.CIRCLE,
                scale: 16, //tamaño
                strokeColor: '#FFFFFF', //color del borde
                strokeWeight: .5, //grosor del borde
                fillColor: '#FF0000', //color de relleno
                fillOpacity: 1 // opacidad del relleno
            },
            map: map,
            title: declaratoria1990_[i]['nombre']
        });
        markers.push(marker);
        o_declaratoria1990.push(marker);
        (function(i, marker) {
            google.maps.event.addListener(marker, 'click', function() {
                let infowindow;
                if (!infowindow) {
                    infowindow = new google.maps.InfoWindow({
                        maxWidth: 600
                    });
                }
                let  informacion = 'Folio:'+ catalogo2006_[i]['folio'] ;
                infowindow.open(map, marker)
                infowindow.setContent(informacion);
                let url = "View/MapaUUO_UV/infoConten/info.php";
                let street = "View/MapaUUO_UV/infoConten/streetView.php";
                let  indice = declaratoria1990_[i]['id'];
                let  lon = declaratoria1990_[i]['Longitud'];
                let  lan = declaratoria1990_[i]['Latitud'];

                //alert(indice)

                $.ajax({
                    data: {id:indice,op:MenuInnmuebles, lon:lon,lan:lan},
                    type: "POST",
                    dataType: "html",
                    url: url,
                    success:function (data) {
                        myFunction(indice,lon,lan);
                        map.setZoom(20);
                        map.setCenter(marker.getPosition());

                    }
                });


                map.setZoom(20);
                map.setCenter(marker.getPosition());
            });
        })(i, marker);
    }

}
function catalogo2006(map){
    markers = [];
    for (i = 0; i < catalogo2006_.length; i++) {
        //console.log(puntos[i])
        var position = new google.maps.LatLng(catalogo2006_[i]['Latitud'], catalogo2006_[i]['Longitud']);
        // /*Marcador*/
        let marker = new google.maps.Marker({
            position: position,
            icon: '/img/m1.png',
            icon: {
                path: google.maps.SymbolPath.CIRCLE,
                scale: 20, //tamaño
                strokeColor: '#FFFFFF', //color del borde
                strokeWeight: .5, //grosor del borde
                fillColor: '#0076F7', //color de relleno
                fillOpacity: 1 // opacidad del relleno
            },
            map: map,
            title: catalogo2006_[i]['folio']
        });
        markers.push(marker);
        o_catalogo2006.push(marker);
        (function(i, marker) {
            google.maps.event.addListener(marker, 'click', function() {
                let infowindow;
                if (!infowindow) {
                    infowindow = new google.maps.InfoWindow({
                        maxWidth: 600
                    });
                }


                let  informacion = 'Folio:'+ catalogo2006_[i]['folio'] ;
                infowindow.open(map, marker)
                infowindow.setContent(informacion);
                let url = "View/MapaUUO_UV/infoConten/info.php";
                let street = "View/MapaUUO_UV/infoConten/streetView.php";
                let  indice = catalogo2006_[i]['id'];
                let  lon = catalogo2006_[i]['Longitud'];
                let  lan = catalogo2006_[i]['Latitud'];

                //alert(indice)
                        myFunction(indice,lon,lan);

                map.setZoom(20);
                map.setCenter(marker.getPosition());

            });
        })(i, marker);
    }

}


// funcion para apagar la capa
function _1981(map) {

    for (let i = 0; i < o_catalogo1981.length; i++) {
        o_catalogo1981[i].setMap(map);

    }
}

function ocultar_1981() {
    _1981(null);
}

// funcion para apagar la capa
function _1984(map) {

    for (let i = 0; i < o_listado1984.length; i++) {
        o_listado1984[i].setMap(map);

    }
}

function ocultar_1984() {
    _1984(null);
}

// funcion para apagar la capa
function _1990(map) {

    for (let i = 0; i < o_declaratoria1990.length; i++) {
        o_declaratoria1990[i].setMap(map);

    }
}

function ocultar_1990() {
    _1990(null);
}

// funcion para apagar la capa
function _2006(map) {

    for (let i = 0; i < o_catalogo2006.length; i++) {
        o_catalogo2006[i].setMap(map);

    }
}

function ocultar_2006() {
    _2006(null);
}

function myFunction(indice,lon,lan){


    let url = "View/MapaUUO_UV/infoConten/info.php";
    //alert(url)



    $.ajax({
        data: {id:indice,op:MenuInnmuebles, lon:lon,lan:lan},
        type: "POST",
        dataType: "html",
        url: url,
        success:function (data) {

            $('.info').html(data)

            infowindow.open(map, marker)


        }
    });


}

function refreshMap() {


}






